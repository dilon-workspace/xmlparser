package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/christopherpfair/xmlparser/pkg/parser"
)

func main() {
	inputFileName := flag.String("input", "", "input file name")
	outputFileName := flag.String("output", "", "output file name")
	getVersion := flag.Bool("version", false, "get version")

	flag.Parse()
	if *getVersion {
		fmt.Println(parser.Version)
		return
	}

	outFileName := *outputFileName
	inFileName := *inputFileName

	if outFileName == "" && inFileName == "" {
		argsWithoutProg := os.Args[1:]
		if len(argsWithoutProg) == 0 {
			fmt.Println("No input file name specified")
			return
		}

		if len(argsWithoutProg) == 1 {
			inFileName = argsWithoutProg[0]
		} else {
			inFileName = argsWithoutProg[0]
			outFileName = argsWithoutProg[1]
		}
	}

	// Assume that if no filename is specified the output filename should end with csv
	if outFileName == "" {
		outFileName = strings.TrimSuffix(inFileName, filepath.Ext(inFileName)) + ".csv"
	}

	if err := parser.ParseXmlToCsv(inFileName, outFileName); err != nil {
		fmt.Println(err)
		return
	}
}
