BINARY_NAME=xmlparser
TEST_DIR = "./pkg/parser"

build:
	GOARCH=amd64 GOOS=linux go build -o ${BINARY_NAME}
	GOARCH=amd64 GOOS=windows go build -o ${BINARY_NAME}.exe 
run:
	./${BINARY_NAME}

build_and_run: build run

clean:
	go clean

test:
	go test ${TEST_DIR}
