package parser

import (
	"encoding/csv"
	"encoding/xml"
	"fmt"
	"os"
	"strings"
)

const Version string = "1.3.0"

type XmlToCsv struct {
	XMLName xml.Name `xml:"CrystalReport"`
	Group   GroupTop `xml:"Group"`
}

type GroupTop struct {
	XMLName xml.Name `xml:"Group"`
	Groups  []Group  `xml:"Group"`
}

type Group struct {
	XMLName     xml.Name        `xml:"Group"`
	GroupHeader GroupHeaderType `xml:"GroupHeader"`
	Details     DetailsGroup    `xml:"Details"`
}

type GroupHeaderType struct {
	XMLName  xml.Name  `xml:"GroupHeader"`
	Sections []Section `xml:"Section"`
}

type DetailsGroup struct {
	XMLName  xml.Name         `xml:"Details"`
	Sections []DetailsSection `xml:"Section"`
}

type DetailsSection struct {
	XMLName   xml.Name  `xml:"Section"`
	Subreport Subreport `xml:"Subreport"`
}

type Subreport struct {
	XMLName xml.Name        `xml:"Subreport"`
	Details DetailsLevelOne `xml:"Details"`
}

type DetailsLevelOne struct {
	XMLName  xml.Name  `xml:"Details"`
	Sections []Section `xml:"Section"`
}

type Section struct {
	XMLName xml.Name `xml:"Section"`
	Fields  []Field  `xml:"Field"`
}

type Field struct {
	XMLName        xml.Name `xml:"Field"`
	FieldName      string   `xml:"FieldName,attr"`
	FormattedValue string   `xml:"FormattedValue"`
}

// NewXmlToCsv creates a new XmlToCsv object
func NewXmlToCsv() *XmlToCsv {
	return &XmlToCsv{}
}

// ParseXmlToCsv parses the xml file to csv
func (XmlToCsv *XmlToCsv) ParseXmlToCsv(inputFileName, outputFileName string) error {
	if err := validateFileNames(inputFileName, outputFileName); err != nil {
		return err
	}

	file, err := os.Open(inputFileName)
	if err != nil {
		return err
	}
	defer file.Close()

	decoder := xml.NewDecoder(file)
	if err := decoder.Decode(XmlToCsv); err != nil {
		return err
	}

	file, err = os.Create(outputFileName)
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	if err := XmlToCsv.WriteCsv(writer); err != nil {
		return err
	}

	return nil
}

// GetMapOfFieldsToColumns returns a map of fields to columns
func GetMapOfFieldsToColumns() map[string]int {
	return map[string]int{
		"Not Used 0!@#$":              0,
		"{Windows_Notes.NOTES_61}":    1,
		"{@Part_ID}":                  2,
		"{Mfg_Part_Master.MPNNUM_49}": 3,
		"Not Used 1!@#$":              4,
		"{Mfg_Part_Master.MPNMFG_49}": 5,
		"{@Qty_Cde}":                  6,
		"{@Description_1}":            7,
		"Not Used 2!@#$":              8,
		"Not Used 3!@#$":              9,
		"Not Used 4!@#$":              10,
		"{@Qty_Per}":                  11,
		"Not Used 5!@#$":              12,
	}
}

// containsOnlyWhiteSpace checks if the string array contains only white space
func containsOnlyWhiteSpace(rowAccumulator [12]string) bool {
	for _, element := range rowAccumulator {
		if !(strings.TrimSpace(element) == "") {
			return false
		}
	}
	return true
}

// partsFieldsAllContainValues checks if all the parts fields contain values
func partsFieldsAllContainValues(rowAccumulator [12]string) bool {
	for i, element := range rowAccumulator {
		if strings.TrimSpace(element) == "" && (i == 1 || i == 2 || i == 3 || i == 5) {
			return false
		}
	}
	return true
}

// WriteCsv writes the csv file
func (XmlToCsv *XmlToCsv) WriteCsv(writer *csv.Writer) (err error) {
	if err := writer.Write([]string{"NHA", "Item Ref", "In House PN", "Mfr PN", "Spec", "Mfr", "Part Type", "Description", "CAGE Code", "Screening Level", "NSN", "Qty", "Note"}); err != nil {
		return err
	}
	mapOfFieldsToColumns := GetMapOfFieldsToColumns()
	rowAccumulator := [12]string{"", "", "", "", "", "", "", "", "", "", "", ""}
	for _, group := range XmlToCsv.Group.Groups {
		rowAccumulator := XmlToCsv.extractGroupHeaderFields(group, mapOfFieldsToColumns, rowAccumulator)
		if !containsOnlyWhiteSpace(rowAccumulator) {
			err = XmlToCsv.extractDetailsFieldsAndWriteRow(group, mapOfFieldsToColumns, &rowAccumulator, writer)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func (*XmlToCsv) writeRow(writer *csv.Writer, rowAccumulator *[12]string) error {
	if !containsOnlyWhiteSpace(*rowAccumulator) {
		if err := writer.Write(rowAccumulator[:]); err != nil {
			return err
		}
	}
	return nil
}

// extractDetailsFields extracts the details fields
func (x *XmlToCsv) extractDetailsFieldsAndWriteRow(group Group, mapOfFieldsToColumns map[string]int, rowAccumulator *[12]string, writer *csv.Writer) (err error) {
	for _, detail := range group.Details.Sections {
		for _, section := range detail.Subreport.Details.Sections {
			for _, field := range section.Fields {
				elementToAdd, ok := mapOfFieldsToColumns[field.FieldName]
				if elementToAdd > 0 && ok {
					trimmedValue := strings.Trim(field.FormattedValue, " ")
					if trimmedValue != "" {
						rowAccumulator[elementToAdd] = trimmedValue
					}
				}
				if !containsOnlyWhiteSpace(*rowAccumulator) && partsFieldsAllContainValues(*rowAccumulator) {
					err = x.writeRow(writer, rowAccumulator)
				}

			}
		}
	}
	return err
}

// extractGroupHeaderFields extracts the fields from the group header
func (*XmlToCsv) extractGroupHeaderFields(group Group, mapOfFieldsToColumns map[string]int, rowAccumulator [12]string) [12]string {
	for _, section := range group.GroupHeader.Sections {
		for _, field := range section.Fields {
			elementToAdd, ok := mapOfFieldsToColumns[field.FieldName]
			if elementToAdd > 0 && ok {
				rowAccumulator[elementToAdd] = strings.Trim(field.FormattedValue, " ")
			}
		}
	}
	return rowAccumulator
}

// validateFileName checks if the file name is valid
func validateFileName(fileName string) error {
	if fileName == "" {
		return fmt.Errorf("File name is empty.")
	}
	return nil
}

// fileExists checks if the file exists
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// validateFileNames checks if the file names are valid
func validateFileNames(inputFileName, outputFileName string) error {
	if err := validateFileName(inputFileName); err != nil {
		return err
	}

	if inputFileName == outputFileName {
		return fmt.Errorf("Input and output file names are the same.")
	}
	if !strings.Contains(inputFileName, ".xml") {
		return fmt.Errorf("Input file name must have .xml extension.")
	}

	if !fileExists(inputFileName) {
		return fmt.Errorf("Input file does not exist.")
	}

	if err := validateFileName(outputFileName); err != nil {
		return err
	}

	if !strings.Contains(outputFileName, ".csv") {
		return fmt.Errorf("Output file name must have .csv extension.")
	}

	return nil
}

// parseXmlToCsv parses the xml file to csv
func ParseXmlToCsv(inputFileName, outputFileName string) error {
	x := NewXmlToCsv()
	if err := x.ParseXmlToCsv(inputFileName, outputFileName); err != nil {
		return err
	}

	return nil
}
