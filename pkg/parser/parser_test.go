package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestParseExmlToCsv tests the ParseExmlToCsv function.
func TestParseExmlToCsv(t *testing.T) {

	// Happy path test.
	err := ParseXmlToCsv("testdata/test.xml", "testdata/test.csv")
	assert.Nil(t, err)

	// Non existent file test.
	err = ParseXmlToCsv("testdata/test_bs.xml", "testdata/test.csv")
	assert.NotNil(t, err)

	// Both files are named the same test
	err = ParseXmlToCsv("testdata/test.xml", "testdata/test.xml")
	assert.NotNil(t, err)

	// Both file names are empty strings
	err = ParseXmlToCsv("", "")
	assert.NotNil(t, err)

	// First file name has the wrong extension
	err = ParseXmlToCsv("testdata/test.csv", "testdata/test.csv")
	assert.NotNil(t, err)

}
